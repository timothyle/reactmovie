import logo from './logo.svg';
import './App.css';
import Spinner from './Components/Spinner/Spinner';
import { BrowserRouter, Routes,Route, createBrowserRouter } from 'react-router-dom';

import MainPage from './Pages/MainPage/MainPage'
import LoginPage from './Pages/LoginPage/LoginPage';
import DetailPage from './Pages/MovieDetailPage/DetailPage';
import UserAdminPage from './Pages/UserAdminPage/UserAdminPage';
import NotFoundPage from './Pages/NotFound404/NotFound404Page';
import Layout from './HOC/Layout';
import SignUpPage from './Pages/SignUpPage/SignUpPage';
import CheckoutPage from './Pages/CheckoutPage/CheckoutPage';
import XacNhanDatVe from './Pages/CheckoutPage/XacNhanDatVe';


function App() {
  return (
    <div className='bg-blue-100'>
    <Spinner/>
     <BrowserRouter>
        <Routes>
            <Route path="/" element={<Layout><MainPage/></Layout>}/>
            <Route path="/login" element={<LoginPage/>}/>
            <Route path="/signup" element={<SignUpPage/>}/>
            <Route path="/detail/:id" element={<Layout><DetailPage/></Layout>}/>
            <Route path="admin/user" element={<Layout><UserAdminPage/></Layout>}/>
            <Route path="confirm" element={<Layout><XacNhanDatVe/></Layout>}/>
            <Route path="*" element={<Layout><NotFoundPage/></Layout>}/>
            <Route path="/checkout/:id" element={<Layout><CheckoutPage/></Layout>}/>
        </Routes>
     </BrowserRouter>
    </div>
  );
}

export default App;
