import React from 'react'
import { useSelector } from 'react-redux'
import { NavLink } from 'react-router-dom';
import { userLocalService } from '../../services/localStorageService';

export default function UserNav() {
    let user = useSelector((state) => {
        return state.userSlice.userInformation;
    });
    let handleLogout = () => {
        userLocalService.remove();
        window.location.href = "/login";
    };
    const renderContent = () => {
        if(user !== null){
          return(
            <navbar>
              <span className='px-5 py-2 ml-3'>{user.hoTen}</span>
              <button className='border-2 rounded border-black px-5 py-2 ml-3' onClick={handleLogout}>Đăng Xuất</button>
            </navbar>
          )
        } else {
          return (
            <div className='nav'>
            <NavLink to="/login"><button className='btn border-2 rounded  border-black  px-5 py-2 ml-3 flex'>Đăng Nhập{" "}</button></NavLink>
            <NavLink to="/signup"><button className='btn border-2 rounded  border-black  px-5 py-2 ml-3 flex'>Đăng Ký</button></NavLink>
            </div>
          )
        }
      }
    
        return <div className='space-x-5'>{renderContent()}</div>
    }
