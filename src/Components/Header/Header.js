import React from 'react'
import { NavLink } from 'react-router-dom'
import UserNav from './UserNav'

export default function Header({ children }) {
  return (
    <div className='flex px-10 py-5 shadow shadow-black justify-center items-center  bg-red-600'>
      <NavLink to="/">
        <span className='text-white text-xl font-medium left-0'>
            HOYTS {children}
        </span>
      </NavLink>
      <div className='px-5 mx-'>
      <UserNav/>
      </div>
    </div>
  )
}
