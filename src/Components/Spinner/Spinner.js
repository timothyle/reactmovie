import React from 'react'
import { useSelector } from 'react-redux'
import HashLoader from "react-spinners/HashLoader"

export default function Spinner() {
    let { isLoading } = useSelector((state) => state.spinnerSlice)
  return isLoading ? (
    <div className='fixed w-screen h-screen bg-black z-50 top-0 left-0 flex items-center justify-center'><HashLoader
    color="#623bff"
    loading
    size={73}
  /></div>
  ) : (
  <></>
  )

}
