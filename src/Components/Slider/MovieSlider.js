import React from "react";
import { Carousel } from "antd";
import { NavLink } from "react-router-dom";

const items = [
    {
        maPhim: 10770,
        tenPhim: "Avengers: Endgame",
        moTa:"After the devastating events of Avengers: Infinity War (2018), the universe is in ruins. With the help of remaining allies, the Avengers assemble once more in order to reverse Thanos' actions and restore balance to the universe.",
        hinhAnh:"https://movienew.cybersoft.edu.vn/hinhanh/avengers-endgame_gp03.jpeg",
        trailer: "https://www.youtube.com/watch?v=TcMBFSGVi1c",
    },
    {
    maPhim: 10705,
    tenPhim: "Black Adam",
    moTa:"Nearly 5,000 years after he was bestowed with the almighty powers of the Egyptian gods - and imprisoned just as quickly - Black Adam is freed from his earthly tomb, ready to unleash his unique form of justice on the modern world.",
    hinhAnh:"https://movienew.cybersoft.edu.vn/hinhanh/black-adam_gp03.png",
    trailer: "https://www.youtube.com/watch?v=mkomfZHG5q4",
    },
    {
    maPhim: 10704,
    tenPhim: "Black Panther: Wakanda Forever",
    moTa:"The nation of Wakanda is pitted against intervening world powers as they mourn the loss of their king T'Challa.",
    hinhAnh:"https://movienew.cybersoft.edu.vn/hinhanh/black-panther-wakanda-forever_gp03.png",
    trailer: "https://www.youtube.com/embed/_Z3QKkl1WyM",
    },
    {
    maPhim: 10439,
    tenPhim: "STRANGE WORLD",
    moTa:"Strange World kể về chuyến phiêu lưu “vượt không gian và thời gian” của gia đình Clades, một gia đình tập hợp toàn những huyền thoại trong làng phiêu lưu khám phá trong chuyến đi khó nhằn nhất của họ. Chuyến đi là cuộc hành trình đến một vùng đất kỳ lạ đầy rẫy những điều bí hiểm cùng những sinh vật chưa bao giờ xuất hiện. Đây cũng có thể sẽ là chuyến hành trình mang tới nhiều điều kỳ lạ nhất của Disney tới khản giả. Nhưng dường như thế giới kì bí ấy có thể còn dễ đương đầu hơn cả những khác biệt và xung đột trong chính gia đình này.",
    hinhAnh:"https://movienew.cybersoft.edu.vn/hinhanh/strange-world_gp03.jpg",
    trailer: "https://youtu.be/j5n3ak0lvRk",
    },
]
const contentStyle= {
    height: '200px',
    color: '#fff',
    lineHeight: '160px',
    textAlign: 'center',
    background: '#364d79',
  };

export default function MovieSlider() {
  return (
    <div className="justify-center items-center">
    <Carousel autoplay dots={true} pauseOnHover={true} pauseOnDotsHover={true} draggable>
        {items.map(item => {
            return (
                <div className="container object-cover bg-black" style={contentStyle}>
                    <img src={item.hinhAnh} alt={item.tenPhim} style={{ width: 500, height:800}} className="d-block w-100 align-middle"/>
                     <div>
                        <h1 className="text-xl">{item.tenPhim}</h1>
                        <h1 className="text-xl p-3 bg-red-500">{item.moTa.slice(0,200)+"..."}</h1>
                        <div className="position-relative">
                            <NavLink to={`/detail/${item.maPhim}`}><button type="primary" size="large" className="btn btn-danger mb-3 mt-3 mr-3">Xem chi tiết</button></NavLink>
                            <button type="primary" size="large" className="btn btn-primary mb-3 ml-3 mt-3">Trailer</button>
                        </div>
                    </div>
                </div>
            );
})}       
    </Carousel>
    </div>
  )
}
