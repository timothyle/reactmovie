import { userService } from "../../services/userService"
import { DAT_VE, GET_USER_INFOR, SET_USER_INFOR, THONG_TIN_DAT_VE } from "../constant/userConstant"
import { userLocalService } from "../../services/localStorageService"
import { message } from 'antd'

export const setLoginAction = (value) => {
    return {
        type: SET_USER_INFOR,
        payload: value,
    }
}
export const setLoginActionService = (userForm, onSuccess) => {
    return (dispatch) => {
        userService
        .postLogIn(userForm)
        .then((res) => {
            message.success("Đăng nhập thành công");
            userLocalService.set(res.data.content);
            console.log(res);
            dispatch({
                    type: SET_USER_INFOR,
                    payload: res.data.content,
            });
            onSuccess();
        })
        .catch((err) => {
            message.error("Đã có lỗi xảy ra");
            console.log(err);
        });
    }
}

export const setPostBookingTicket = (dataVeUser, onSuccess) => {
    return (dispatch) => {
        userService
        .postBookVe(dataVeUser)
        .then((res) => {
            message.success("Đặt Vé Thành Công")
            dispatch({
                    type: DAT_VE,
                    payload: res.data.content,
            });
        })
        .catch((err) => {
            message.error("Đã có lỗi xảy ra");
            console.log(err);
        });
    }
}
export const setSignUpActionService = (userSignUpForm, onSuccess) => {
        return (dispatch) => {
    userService
    .postSignUp(userSignUpForm)
    .then((res) => {
        userLocalService.set(res.data.content);
        message.success("Đăng Ký thành công");
        console.log(res);
        dispatch({
                type: GET_USER_INFOR,
                payload: res.data.content,
                maNhom: "GP03"
        });
        onSuccess();
    })
    .catch((err) => {
        message.error("Tên đăng nhập hoặc email đã tồn tại! Thử cái khác đê");
        console.log(err);
    });
}
}

export const setThongTinVeTaiKhoan = (taiKhoan, onSuccess) => {
    return (dispatch) => {
        userService
        .getDanhSachVePhimDaDat(taiKhoan)
        .then((res) => {
            console.log(res);
            dispatch({
                    type: THONG_TIN_DAT_VE,
                    payload: res.data.content,
            });
            onSuccess();
        })
        .catch((err) => {
            message.error("Đã có lỗi xảy ra");
            console.log(err);
        });
    }
}