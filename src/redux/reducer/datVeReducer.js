import { GET_THONG_TIN_VE_DAT } from '../constant/userConstant';

const initialState = {
    danhSachGheDuocChon:[]
}

export const QuanLyDatVeReducer = (state = initialState, action) => {
    switch (action) {
        case GET_THONG_TIN_VE_DAT: {
            state.danhSachGheDuocChon = action;
          return {...state.danhSachGheDuocChon}
        }
        default: return {...state}
    }
}

