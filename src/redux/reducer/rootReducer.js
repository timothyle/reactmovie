import { combineReducers } from "redux";
import {datVeReducer} from "./datVeReducer";
import  {userReducer}  from "./userReducer"


export const rootReducer = combineReducers( userReducer, datVeReducer);