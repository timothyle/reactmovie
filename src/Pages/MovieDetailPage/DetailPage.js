import React, {useState, useEffect} from 'react'
import { NavLink, useParams } from 'react-router-dom'
import { movieService } from '../../services/movieService';
import { CircularProgressbar } from 'react-circular-progressbar';
import 'react-circular-progressbar/dist/styles.css';
import ReactPlayer from 'react-player/youtube';
import MovieTabItem from '../MainPage/MovieTabs/MovieTabItem';
import { Tabs } from 'antd';
import TabPane from 'antd/es/tabs/TabPane';
import moment from 'moment';
const onChange = (key) => {
  console.log(key)
}


export default function DetailPage(params) {
    params = useParams();
    const [detailLichChieuMovie, setdetailLichChieuMovie] = useState([])
    useEffect(()=>{
        movieService
        .getThongTinLichChieuPhim(params.id)
        .then((res) => {
                console.log(res)
                setdetailLichChieuMovie(res.data.content);
              })
              .catch((err) => {
               console.log(err);
              });
    },[]);
    //đánh giá nè
    const value = detailLichChieuMovie.danhGia;
    // function render ra hệ thống rạp có phim nè
  return (
    <div className='container'>
        <div className='row'>
          <div className='col-6 col-md-4 col-sm-6'>
              <img src={detailLichChieuMovie.hinhAnh} style={{ width:400, height:500 }} alt="" />
          </div>
          <div className='col-8 col-md-8 col-sm-6'>
            <div className='player-wrapper'>
              <ReactPlayer
                width='100%'
                height='100%'
                className='react-player'
                url={detailLichChieuMovie.trailer}/>
            </div>
          </div> 
          </div>
          <div className="bg-blue-200">
            <Tabs
            centered
              defaultActiveKey="1"
              onChange={onChange}
              items={[
                {
                  label: `Thông Tin Lịch Chiếu Phim`+" "+`${detailLichChieuMovie.tenPhim}` ,
                  key: '1',
                  children: <Tabs tabPosition={'left'}>
                  {detailLichChieuMovie.heThongRapChieu?.map((heThongRapChieuPhim, index) => {  
                    return <TabPane
                    tab={<div>
                      <img src={heThongRapChieuPhim.logo} alt="" className='rounded-full' width="60"/>
                      <h1>{heThongRapChieuPhim.tenHeThongRap}</h1>
                    </div>}
                    key={index}>
                          {heThongRapChieuPhim.cumRapChieu?.map((cumRapPhim,index) => {
                            return <div className='mt-5' key={index}>
                              <div className='flex flex-row'>
                                <img src={cumRapPhim.hinhAnh} alt="" style={{width:60,height:60}}/>
                                <div className='ml-2 container'>
                                  <h1 style={{fontSize:20, fontWeight:'bold',color:'red'}}>{cumRapPhim.tenCumRap}</h1>
                                  <h2 className='text-blue-700' style={{marginTop:0}}>DC: {cumRapPhim.diaChi}</h2>
                               </div>
                              </div>
                              <div className='grid grid-cols-4'>
                                {cumRapPhim.lichChieuPhim?.map((lichChieu,index) => {
                                  return <NavLink to={`/checkout/${lichChieu.maLichChieu}`} key={index} className="col-span-1 text-blue-800 font-bold text-2xl">
                                    Suất {moment(lichChieu.ngayChieuGioChieu).format('hh:mm A')}
                                  </NavLink>
                            
                                })}
                              </div>
                            </div>

                          })}
                    </TabPane>
  
                  })}
              </Tabs>,
                },
                {
                  label: `Mô Tả Về Phim`,
                  key: '2',
                  children: <h2 className='text-black p-3'>
                  {detailLichChieuMovie.moTa}
                </h2>
                },
                {
                  label: `Đánh Giá`,
                  key: '3',
                  children: <div style={{ width:300, height:300 }} className='text-black'>Đánh giá <CircularProgressbar className='p-5' value={value} maxValue={10} text={`${value * 1} Điểm`} /></div> ,
                },
              ]}
            />
            
          </div>
      </div>
  )
    }

