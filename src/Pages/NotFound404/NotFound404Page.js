import React from 'react'
import Header from '../../Components/Header/Header'

export default function NotFoundPage() {
  return (
   <div>
    <div className='flex justify-center items-center h-screen w-screen'>
        <img style={{display: 'block', WebkitUserSelect: 'none', margin: 'auto', cursor: 'zoom-in', backgroundColor: 'hsl(0, 0%, 90%)', transition: 'background-color 300ms'}} src="https://i.redd.it/dtljzwihuh861.jpg" width={1532} height={862} />
    </div>
    </div>
  )
}
