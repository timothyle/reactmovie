import { Button, Form, Input,Checkbox } from 'antd';
import React, { useReducer } from 'react'
import { useDispatch } from 'react-redux';
import { NavLink, useNavigate } from 'react-router-dom';
import Lottie from "lottie-react";
import bg_animation from "../../assets/background.json"
import { setLoginActionService } from '../../redux/actions/userAction';

export default function LoginPage() {
    let navigate = useNavigate();
    let dispatch = useDispatch();
    const [ignored, forceUpdate] = useReducer(x => x + 1, 0);
  const onFinishReduxThunk = (value) => {
      let onNavigate = () => {
          setTimeout(() => {
              navigate(-1, forceUpdate);
          },1000);
      }
      dispatch(setLoginActionService (value, onNavigate))
  };
  
  const onFinishFailed = (errorInfo) => {
      console.log('Failed:', errorInfo);
    };
return (
  <div className='w-screen h-screen flex justify-center items-center'>
      <div className='container p-5 flex'>
          <div className='h-full w-1/2'>
              <Lottie animationData={bg_animation} loop={true} />
          </div>
          <div className='h-full w-1/2'>
          <Form
          name="basic"
          labelCol={{
              span: 8,
          }}
          wrapperCol={{
              span: 24,
          }}
          initialValues={{
              remember: true,
          }}
          onFinish={onFinishReduxThunk}
          onFinishFailed={onFinishFailed}
          autoComplete="off"
          layout='vertical'
          >
          <Form.Item
              label="Username"
              name="taiKhoan"
              rules={[
              {
                  required: true,
                  message: 'Please input your username!',
              },
              ]}
          >
              <Input />
          </Form.Item>

          <Form.Item
              label="Password"
              name="matKhau"
              rules={[
              {
                  required: true,
                  message: 'Please input your password!',
              },
              ]}
          >
              <Input.Password />
          </Form.Item>
          <Form.Item
          className='text-center'
              wrapperCol={{
              span: 24,
              }}
          >
            <Form.Item>
            <Form.Item name="remember" valuePropName="checked" noStyle>
                <Checkbox>Remember me</Checkbox>
                </Form.Item>

                <NavLink className="login-form-forgot" to="/">
                Forgot password
                </NavLink>
            </Form.Item>
            <Form.Item>
                    <NavLink to="/signup">Register Now!</NavLink>
            </Form.Item>
              <Button type="primary" className='text-white' danger htmlType="submit">
              Submit
              </Button>
          </Form.Item>
          </Form>
          </div>
      </div>
  </div>
)
}

