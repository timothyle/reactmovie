import { Tabs } from 'antd';
import Item from 'antd/es/list/Item';
import React, { useEffect, useState } from 'react'
import { userService } from '../../services/userService';
const onChange = (key) => {
  console.log(key)
}


export default function XacNhanDatVe({}) {
  const [movieArr,setMovieArr] = useState([])
  useEffect (() =>{
          userService
          .getDanhSachVePhimDaDat()
          .then((res) => {
              console.log(res)
              setMovieArr(res.data.content)
          })
          .catch((err) => {
              console.log(err)
          })
  },[]);
  let refreshPage = () =>{
    window.location.reload()
  }
    console.log("movieARr",movieArr)
    
  return (
    <div className="container mt-5">
    
    <div className="row d-flex justify-content-center">
        
        <div className="col-md-7">
            
            <div className="card p-3 py-4">
                
                <div className="ml-auto mr-auto">
                    <img src="https://www.shutterstock.com/image-vector/user-icon-trendy-flat-style-260nw-418179865.jpg" c width={100} className="rounded-circle" />
                </div>
                
                <div className="text-center mt-3">
                    <span className="bg-secondary p-1 px-4 rounded text-white text-xl">{movieArr.hoTen}</span>
                    <div className='text-black'>
                      Các thông tin ghế đã đặt:
                      <div>
                        {movieArr.thongTinDatVe?.map((item,index)=>{
                          return (
                          <div key={index}>
                            <div className='bg-slate-400'>Tên Phim: {item.tenPhim}</div>
                            
                              {item.danhSachGhe?.map((ghe,index)=>{
                                return (
                                <div key={index}>
                                  Vị trí ghế: <span className='text-red-500 text-xl'>{ghe.tenGhe}</span>
                                  <div>Vị Trí Rạp: <span className='text-yellow-500 text-xl '>{ghe.tenRap}</span></div>
                                  <div>Tên Hệ Thống Rạp: <span className='text-blue-500 text-xl '>{ghe.tenHeThongRap}</span></div>
                                </div>
                                )
                              })}
                          </div>
                          )
                        })}
                      </div>
                    </div>
                    <div className="px-4 mt-1 text-black">
                        <p className="fonts text-black"></p>
                    </div>
                    <div className="buttons">
                        <button className="btn btn-outline-primary px-4" onClick={refreshPage}>Cập nhật</button>
                    </div>
                </div>   
            </div>
        </div>
    </div>
</div> 
  )
}
