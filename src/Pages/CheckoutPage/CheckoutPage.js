import React, { Fragment, useEffect, useState,useReducer } from 'react'
import { USER_LOGIN } from '../../services/localStorageService'
import { useNavigate, useParams } from 'react-router-dom'
import { message } from 'antd'
import { movieService } from '../../services/movieService';
import { CloseOutlined } from '@ant-design/icons';
import { useDispatch } from 'react-redux';
import { layThongTinGhe } from '../../redux-toolkit/slice/userSlice';
import { danhSachCapNhat } from '../../redux-toolkit/slice/userSlice'
import { setPostBookingTicket } from '../../redux/actions/userAction';
import { userService } from '../../services/userService';
import { DAT_VE } from '../../redux/constant/userConstant';
import { ThongTinDatVe } from './ThongTinDatVe';
import XacNhanDatVe from './XacNhanDatVe';
const _ = require("lodash");

let danhSachGheCapNhat = danhSachCapNhat;
console.log("1",danhSachGheCapNhat);

export default function CheckoutPage(params) {
    const [ignored, forceUpdate] = useReducer(x => x + 1, 0);
    let userJson = localStorage.getItem(USER_LOGIN);
    const thongTin = JSON.parse(userJson);
    let dispatch = useDispatch();
    let navigate = useNavigate();
    //lấy id đã chuyền vào
    params = useParams();
    const [detailDatVe, setdetailDatVe] = useState([])
    useEffect(()=>{
        movieService
        .getThongTinVePhim(params.id)
        .then((res) => {
                console.log(res)
                setdetailDatVe(res.data.content);
              })
              .catch((err) => {
               console.log(err);
              });    
    },[]);
    const chiTiet = detailDatVe.thongTinPhim;
    const danGhe = detailDatVe.danhSachGhe;
    if(!localStorage.getItem(USER_LOGIN)) {
        return setTimeout(() => {
            navigate("/login", { replace: true });
        },300,message.success("đăng nhập vô ạ"))
    }
     const renderViTriGhe = () => {
        return danGhe?.map((ghe,index) => {
            let classGheVip = ghe.loaiGhe === 'Vip' ? 'gheVip' : '';
            let classGheDaDat = ghe.daDat === true ? 'gheDaDat' : '';
            let classGheDangDat = '';
            let indexDD = danhSachCapNhat.findIndex(gheDaBook => gheDaBook.maGhe === ghe.maGhe);
            if (indexDD != -1){
                classGheDangDat = 'gheDangDat';
            }
            return <Fragment key={index}>
            <button onClick={() => {
                forceUpdate()   
                dispatch(layThongTinGhe(ghe,forceUpdate))
            }}     disabled={ghe.daDat} className={`ghe ${classGheVip} ${classGheDaDat} ${classGheDangDat} text-center`} key={index}>
                    {ghe.daDat ? <CloseOutlined style={{marginBottom:5}}/> : ghe.stt}
                </button>
                
                {(index + 1) % 16 === 0 ? <br/> : ''}
                
                </Fragment>
                
        })
     }
     
// tạo danh sách ghế
  return (
    <div className style={{minHeight:"100vh"}}>
        <div className='grid grid-cols-12'>
            <div className='col-span-9'>
                <div className='flex flex-col items-center mt-2'>
                    Màn Hình Chiếu
                    <div className='bg-black' style={{ width:'80%',height:15}}>
                    </div>
                    <div>
                        {renderViTriGhe()}
                        
                    </div>
                </div>
                Tình trạng của ghế
                <div className='flex flex-row items-center'>
                    <div className='ml-4 mr-2' style={{width:35,height:35,backgroundColor:"red",borderRadius:10}}>
                    </div>
                    <span className='ml-2 mr-2'>Đã Đặt</span>
                    <div className='ml-4 mr-2' style={{width:35,height:35,backgroundColor:"rgb(123, 122, 122)",borderRadius:10}}>
                    </div>
                    <span className='ml-2 mr-2'>Ghế Thường</span>
                    <div className='ml-4 mr-2' style={{width:35,height:35,backgroundColor:"orange",borderRadius:10}}>
                    </div>
                    <span className='ml-2 mr-2'>Ghế Vip</span>
                </div>
            </div>
            <div className='col-span-3 min-h-screen'>
                <h3 className='text-green-400 text-center text-2xl'>
                   Thông Tin Đặt Vé
                </h3>
                <hr />
                <h3 className='text-xl text-black'>Tên Phim: {chiTiet?.tenPhim}</h3>
                <p className=' text-black text-lg'><p className='font-bold'>Địa Điểm: {chiTiet?.tenCumRap} </p> </p>
                <p className=' text-black text-lg'><p className='font-bold'>Giờ Chiếu: {chiTiet?.gioChieu}</p> </p>
                <p className=' text-black text-lg'><p className='font-bold'>Ngày chiếu: {chiTiet?.ngayChieu} </p> </p>
                <p className=' text-black text-lg'><p className='font-bold'>Tại: {chiTiet?.tenRap} </p> </p>
                <img src={chiTiet?.hinhAnh}  style={{width:100,height:100}} alt="" />
                <div className='flex flex-row my-2'>
                    <div className='w-4/5'>
                        <span className=' text-red-600 text-lg'>
                            Số Lượng Ghế Chọn: {_.sortBy(danhSachCapNhat,['stt']).map((gheDaBook,index)=>{
                                return <div className='text-green-500' key={index}>{gheDaBook.stt}</div>
                            })}
                        </span>
                    </div>
                        <div className='text-blue-600 text-xl'>
                            Tổng Tiền: 
                            <div className='text-lg text-green-500'>
                                {danhSachCapNhat.reduce((tongTien,ghe,index)=>{
                                    return tongTien += ghe.giaVe},0).toLocaleString()}  Đồng
                            </div>
                        </div>
                    <hr/>
                </div>
                <hr />
                <div className='my-1 text-black'>
                    <i className='font-bold'>Họ và Tên:</i><br/>
                    {thongTin.hoTen}
                </div>
                <hr/>
                <div className='my-1 text-black'>
                    <i className='font-bold'>Số điện thoại của bạn</i><br/>
                    {thongTin.soDT}
                </div>
                <hr/>
                <div className='my-1 text-black'>
                    <i className='font-bold'>Email của bạn hiện tại</i><br/>
                    {thongTin.email}
                </div>
                <hr/>
                <br/>
                <div onClick={()=>
                {
                    const thongTinDatVe = new ThongTinDatVe();
                    thongTinDatVe.maLichChieu = params.id
                    thongTinDatVe.danhSachVe = danhSachCapNhat;
                    <XacNhanDatVe thongTinDatVe={thongTinDatVe}/>
                    console.log(thongTinDatVe);
                    dispatch(setPostBookingTicket(thongTinDatVe));
                    navigate("/confirm",thongTinDatVe)
                }} style={{cursor:'pointer'}} className='btn btn-success font-bold w-full text-black'>
                    Đặt vé ngay
                    
                </div>
            </div>
        </div>
        
    </div>
  )
}


