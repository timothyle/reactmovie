import { Tag } from "antd";

export const columnUser = [
    {
        title: "Tài khoản",
        dataIndex: "taiKhoan",
        key: "taiKhoan",
    },
    {
        title: "Họ Tên",
        dataIndex: "hoTen",
        key: "hoTen",
    },
    {
        title: "Email",
        dataIndex: "email",
        key: "email",
    },
    {
        title: "Loại khách",
        dataIndex: "maLoaiNguoiDung",
        key: "maLoaiNguoiDung",
        render: (text) => {
            if(text === "QuanTri"){
                return <Tag color="red">Quản Trị</Tag>
            }else{
                return <Tag color="blue">Khách Hàng</Tag>
            }
        }
    },
    {
        title: "Thao tác",
        dataIndex: "action",
        key: "action"
    }
]   ;