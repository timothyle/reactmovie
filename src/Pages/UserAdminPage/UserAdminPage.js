import React, { useEffect, useState } from 'react'
import { adminService } from '../../services/adminService'

import { message, Table } from 'antd';
import { columnUser } from './utils';
import Spinner from '../../Components/Spinner/Spinner';
import { Header } from 'antd/es/layout/layout';
export default function UserAdminPage() {
    const [userArr, setUserArr] = useState([]);
    console.log(userArr)
    useEffect(() => {
      
      let handleDeleteUser = (idUser) =>{
        adminService
        .xoaNguoiDung(idUser)
        .then((res) => {
                console.log(res);
                message.success("Xóa người dùng thành công");
                fetchUserList();
              })
              .catch((err) => {
                message.error(err.response.data.content);
               console.log(err);
               fetchUserList();
              });
      }

      let fetchUserList = () => {
        adminService
        .layDanhSachNguoiDung()
        .then((res) => {
                // console.log(res);
                let userList = res.data.content.map((item)=>{
                  return {
                    ...item,
                    action: (
                      <>
                      <button onClick={() => {handleDeleteUser(item.taiKhoan)}} className='px-2 py-1 rounded bg-red-500 text-white'>
                        Xóa
                      </button>
                      <button className='px-2 py-1 rounded bg-blue-500 text-white'>
                        Sửa
                      </button>
                      </>
                    )
                  }
                });
                setUserArr(userList);
              })
              .catch((err) => {
               console.log(err);
              })
              ;
      };
      fetchUserList();
        ;
    },[])
  return (
    <div className='container mx-auto'>
        <Header/>
        <Table columns={columnUser} dataSource={userArr} />
    </div>
  )
}
