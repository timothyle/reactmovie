import { Button, Form, Input, Select} from 'antd';
import React from 'react'
import { maNhom } from '../../services/configURL';
import { useDispatch } from 'react-redux';
import { useNavigate } from 'react-router-dom';
import Lottie from "lottie-react";
import createAccount from "../../assets/createAccount.json"
import { setSignUpActionService } from '../../redux/actions/userAction';


export default function SignUpPage() {
    let navigate = useNavigate();
    let dispatch = useDispatch();
    const { Option } = Select;
    const onFinish = (values) => {
      values.maNhom = maNhom;
      let onNavigate = () => {
          setTimeout(() => {
            values.maNhom = "GP03";
              navigate("/");
          },1000)
      }
      
      dispatch(setSignUpActionService(values,onNavigate))
  };
;
  const onFinishFailed = (errorInfo) => {
      console.log('Lỗi rồi:', errorInfo.response);
    };
return (
  <div className='w-screen h-screen flex justify-center items-center'>
      <div className='container p-5 flex'>
          <div className='h-full w-1/2'>
              <Lottie animationData={createAccount} loop={true} />
          </div>
          <div className='h-full w-1/2'>
          <Form
          name="Sign Up"
          labelCol={{
              span: 8,
          }}
          wrapperCol={{
              span: 24,
          }}
          initialValues={{
              remember: true,
          }}
          onFinish={onFinish}
          autoComplete="off"
          layout='vertical'
          >
          <Form.Item
              label="Tên Đăng Nhập"
              name="taiKhoan"
              rules={[
              {
                  required: true,
                  message: 'Please input your username!',
              },
              ]}
          >
              <Input />
          </Form.Item>
          <Form.Item
              label="Mật Khẩu"
              name="matKhau"
              rules={[
              {
                  required: true,
                  message: 'Please input your password!',
                  type: "password",
              },
              ]}
          >
              <Input.Password />
          </Form.Item>
          <Form.Item
          label="email"
        className="col-span-6"
        name="email"
        rules={[
          {
            required: true,
            message: "Please input your username!"
          }
        ]}>
          <Input placeholder="Nhập email" name="email" required />
      </Form.Item>
      <Form.Item
      label="Số điện thoại"
        className="col-span-6"
        name="soDT"
        rules={[
          {
            required: true,
            message: "Please input your username!"
          }
        ]}>
          <Input placeholder="Nhập số điện thoại" name="soDT" required type="number" />
      </Form.Item>
      <Form.Item
          label="Họ và tên"
        className="col-span-6"
        name="hoTen"
        rules={[
          {
            required: true,
            message: "Please input your fullname!"
          }
        ]}>
          <Input placeholder="Nhập họ và tên" name="hoTen" required />
      </Form.Item>
          <Form.Item
          className='text-center'
              wrapperCol={{
              span: 24,
              }}
          >
              <Button type="primary" className='text-black' danger htmlType="submit">
              Đăng Ký
              </Button>
          </Form.Item>
          </Form>
          </div>
      </div>
  </div>
)
}

