import React from 'react'

import { Card, Popover } from 'antd';
import { NavLink } from 'react-router-dom';



export default function MovieList( { movieArr }) {
    const renderMovieList = () => {
    return movieArr.slice(0, 20).map((item) => {
        const info = (
            <div className='container-xl border-x-red-600'>
                <h1 className='text-xl font-weight-bold'>{item.tenPhim}</h1>
                <h3>{item.moTa.slice(0,100)}</h3>
            </div>
        )
        return (
            <NavLink to={`/detail/${item.maPhim}` }>  
                <Card
            hoverable
            style={{ width: "100%",height:"100%"}}
            cover={<img 
                alt="example" 
            src={item.hinhAnh} className="h-100 object-cover"/>}
        >
                    <div className='btn btn-danger'>
                        Chi Tiết
                    </div>
                </Card>
            </NavLink>   
  )
})
    }
return <div className='grid grid-cols-5 gap-5'>
    {renderMovieList()}
    
    </div> 
}