import React, { useEffect, useState } from 'react'
import MovieSlider from '../../Components/Slider/MovieSlider';
import { movieService } from '../../services/movieService';
import MovieList from './MovieList/MovieList';
import MovieTabs from './MovieTabs/MovieTabs';

export default function MainPage() {
  const [movieArr,setMovieArr] = useState([])
  useEffect (() =>{
          movieService
          .getDanhSachPhim()
          .then((res) => {
              console.log(res)
              setMovieArr(res.data.content)
          })
          .catch((err) => {
              console.log(err)
          })
  },[]); 
return (
  <div className='container mx-auto'>
    <MovieSlider movieArr={movieArr}/>
    <br/>
    <MovieList movieArr={movieArr}/>
    <br/>
    <MovieTabs/>     
    <br/>
    <br/>
    <br/>
    <br/>
    <br/>
    <br/>
    <br/>
    <br/>
  </div>
)
}
