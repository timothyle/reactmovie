import { createSlice } from '@reduxjs/toolkit'
import { message } from 'antd';
import { userLocalService } from '../../services/localStorageService';

const initialState = {
    userInformation: userLocalService.get(),
    userGetInformation: userLocalService.get(),
    danhSachGheDuocChon:[],
};

export const danhSachCapNhat =  [];
const userSlice = createSlice({
  name: "userSlice",
  initialState,
  reducers: {
    setUserInformation: (state, action) => {
        state.userInformation = action.payload;
    },
    getUserInformation: (state,action) => {
      state.userGetInformation = action.payload;
      console.log(state.userGetInformation)
    },
    layThongTinGhe: (state,action) => {
      state.danhSachGheDuocChon = action.payload;
      let indexDD = danhSachCapNhat.findIndex(gheDaBook => gheDaBook.maGhe === action.payload.maGhe);
      console.log(indexDD)
      if ( indexDD !== -1 ){
        danhSachCapNhat.splice(indexDD,1)
        message.error("Bạn đã bỏ chọn ghế này")
        console.log("already",danhSachCapNhat)
      }else{
      danhSachCapNhat.push(action.payload);
      message.success("Bạn đã thêm vị trí mới thành công")
      console.log("success",danhSachCapNhat)
      } return danhSachCapNhat;
    }
  }
});

export const {setUserInformation, getUserInformation, layThongTinGhe} = userSlice.actions;



export default userSlice.reducer