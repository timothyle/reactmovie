import { https } from "./configURL"

export const movieService = {
    getDanhSachPhim: () => {
        return https.get("api/QuanLyPhim/LayDanhSachPhim?maNhom=GP03")
    },
    getPhimTheoHeThongRap: () => {
        return https.get("api/QuanLyRap/LayThongTinLichChieuHeThongRap?maNhom=GP03")
    },
    getThongTinChiTietPhim: (id) => {
        return https.get(`api/QuanLyPhim/LayThongTinPhim?MaPhim=${id}`)
    },
    getThongTinLichChieuPhim: (id) => {
        return https.get(`api/QuanLyRap/LayThongTinLichChieuPhim?MaPhim=${id}`)
    },
    getThongTinVePhim: (id) => {
        return https.get(`api/QuanLyDatVe/LayDanhSachPhongVe?MaLichChieu=${id}`)
    },
}