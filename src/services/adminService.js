import { https } from "./configURL"

export const adminService = {
    layDanhSachNguoiDung: () => {
        return https.get("api/QuanLyNguoiDung/LayDanhSachNguoiDung?MaNhom=GP03");
    },
    xoaNguoiDung: (idUser) => {
        return https.get(`api/QuanLyNguoiDung/XoaNguoiDung?TaiKhoan=${idUser}`);
    }
}