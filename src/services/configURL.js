
import axios from "axios";

import {store_toolkit} from '../index'

import { userLocalService } from "./localStorageService"
import { setLoadingOff, setLoadingOn } from "../redux-toolkit/slice/spinnerSlice";

export const BASE_URL = "https://movienew.cybersoft.edu.vn"

export const LOGIN_TOKEN = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ0ZW5Mb3AiOiJCb290Y2FtcCBTw6FuZyAwNSIsIkhldEhhblN0cmluZyI6IjI4LzA1LzIwMjMiLCJIZXRIYW5UaW1lIjoiMTY4NTIzMjAwMDAwMCIsIm5iZiI6MTY2MjMxMDgwMCwiZXhwIjoxNjg1Mzc5NjAwfQ.FtGbsXl4qyqTRfJrunro0mQ7b-tNs8EWbhb7JDTzloEn"

export const maNhom = "GP03";

export const createConfig = () => {
return {
    TokenCybersoft: LOGIN_TOKEN,
    Authorization: "Bearer " + userLocalService.get()?.accessToken, 
}
};

export const https = axios.create({
    baseURL : BASE_URL,
    headers: createConfig(),
});

https.interceptors.request.use(
    function(config) {
        store_toolkit.dispatch(setLoadingOn());
        console.log("request");
        return config;
    },
    function (error) {
        return Promise.reject(error);
    }
)
https.interceptors.response.use(
    function(response) {
        store_toolkit.dispatch(setLoadingOff());
        console.log("response");
        return response;
    },
    function (error) {
        return Promise.reject(error);
    }
)