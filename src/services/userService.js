import axios from "axios";
import { BASE_URL, createConfig } from "./configURL";
const maNhom = "GP03"


export const userService = {
    postLogIn: (dataUser) => {
        return axios ({
            url: `${BASE_URL}/api/QuanLyNguoiDung/DangNhap`,
            method: "POST",
            data: dataUser,
            headers: createConfig(),
        });
    },
    postBookVe: (dataUser) => {
        return axios ({
            url: `${BASE_URL}/api/QuanLyDatVe/DatVe`,
            method: "POST",
            data: dataUser,
            headers: createConfig(),
        });
    },
    postSignUp: (dataSignUpUser) => {
        return axios ({
            url: `${BASE_URL}/api/QuanLyNguoiDung/DangKy`,
            method: "POST",
            data: dataSignUpUser,
            headers: createConfig(),
            maNhom: maNhom,
        })
    },
    getDanhSachPhim: () => {
        return axios ({
            url: `${BASE_URL}/api/QuanLyPhim/LayDanhSachPhim`,
            method: "GET",
            headers: createConfig(),
        })
    },
    getDanhSachVePhimDaDat: () => {
        return axios ({
            url: `${BASE_URL}/api/QuanLyNguoiDung/ThongTinTaiKhoan`,
            method: "POST",
            headers: createConfig(),
        })
    }
};
