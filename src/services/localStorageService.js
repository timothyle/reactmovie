export const USER_LOGIN = 'USER_LOGIN';

export const VE_INFO = 'VE_INFO'

export const userLocalService = {
    set: (userData) => {
        let userJson = JSON.stringify(userData);
        localStorage.setItem(USER_LOGIN,userJson);
    },
    get: () => {
        let userJson = localStorage.getItem(USER_LOGIN);
        if (userJson != null) {
            return JSON.parse(userJson);
        } else {
            return null;
        }
    },
    remove: () => {
        localStorage.removeItem(USER_LOGIN)
    }
}

export const getVeLocalService = {
    set: (userVeData) => {
        let veJson = JSON.stringify(userVeData);
        localStorage.setItem(VE_INFO,veJson);
    },
    get: () => {
        let veJson = localStorage.getItem(VE_INFO);
        if (veJson != null) {
            return JSON.parse(veJson);
        } else {
            return null;
        }
    },
}